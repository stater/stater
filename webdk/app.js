/**
 * Application Bootstrap
 * This bootstrap is executed when application is ready to go but need some touch before
 * running. Ensure to trigger the callback or the app will never started.
 * @param complete
 */

"use strict";

module.exports = function bootApp ( complete ) {

    complete();
}