module.exports = {
    'list' : {
        path : 'get /user',
        view : 'user-list'
    },

    'view' : {
        path : 'get /user/:id',
        view : 'user-view'
    },

    'add' : {
        path       : 'post /user',
        view       : 'confirm-signup',
        controller : 'UserController'
    },

    'put' : {
        path       : 'put /user/:id',
        view       : 'user-update',
        controller : 'UserController'
    }
}