#! /usr/bin/env node

'use strict';

/* Script Loader */
require('singclude');

/* Loading Public Dependencies */
include.global({
    File : 'fs',
    Exec : 'vm',
    Bash : 'child_process',
    Path : 'path'
});

/* Loading Helpers */
include('server/patches/jsfix');
include('server/helpers/cli');

/* Loading Required Modules */
var prompt = require('inquirer').prompt;

/* Loading Core Configurations */
var pkgInfo = require('./package.json');

/* Creating CLI Hanlders */
new CLI({
    /* Setting up CLI Configs */
    name    : 'StaterJS',
    about   : pkgInfo.description,
    version : pkgInfo.version,
    usage   : 'stater [command] [options...]',

    /* Commands */

    /* Start Application */
    start : {
        type : 'command',
        info : 'Start application in production mode',
        help : 'stater start [options...]\r\n',

        exec : startApp
    },

    /* Restart Application */
    restart : {
        type : 'command',
        info : 'Restart application',
        help : 'stater restart\r\n',

        exec : restartApp
    },

    /* Exit Application */
    stop : {
        type : 'command',
        info : 'Stop application',
        help : 'stater stop\r\n',

        exec : stopApp
    },

    /* Develop Application */
    develop : {
        type : 'command',
        info : 'Start application in development mode, force enabling livereload, no-cache, etc',
        help : 'stater develop [options...]\r\n',

        exec : develApp
    },

    /* Setup Application */
    setup : {
        type : 'command',
        info : 'Setup application using iteractive wizard',
        help : 'stater setup\r\n',

        exec : setupApp
    },

    /* Get/Set Configuration */
    config : {
        type : 'command',
        info : 'Get/Set Configurations',
        help : 'stater config [property=value...] or stater config [property]\r\n',

        exec : configApp
    },

    /* Maintenance */
    'put-down' : {
        type : 'command',
        info : 'Put application in maintenance mode',
        help : 'stater put-down\r\n',

        exec : liftDownApp
    },

    /* Release Maintenance */
    'put-live' : {
        type : 'command',
        info : 'Release the maintenance mode',
        help : 'stater put-live\r\n',

        exec : liftUpApp
    },

    /* Application Updater */
    update : {
        type : 'command',
        info : 'Get application update from the git.\r\nAutomatically put to maintenance mode and live back when done',
        help : 'stater update [options...]',

        exec : updateApp
    },

    /* Options */
    port         : {
        info : 'Assign listening port to the application.'
    },
    host         : {
        info : 'Assign listening host to the application.'
    },
    simple       : {
        info : 'Start application without backend stuff.\r\nImprove speed for template development or static HTML.'
    },
    'write-host' : {
        info : 'Write host on start to /etc/hosts (Mac/Linux) or {SYSTEM}\\Drivers\\etc\\hosts (Win).'
    },
    verbose      : {
        info : 'Log all error and warnings',
        defl : false
    },
});

/* App Starter */
function startApp ( arg ) {
    var swd = __dirname, cwd = process.cwd();

    Bash.exec('cd ' + swd + ' && node server/www.js --client-dir="' + cwd + '" --host=localhost --port=8003 --verbose --reloads --production --avoid-session', function ( err, std ) {
        console.log(std);
    });
}

/* App Restarter */
function restartApp ( arg ) {

}

/* App Stopper */
function stopApp ( arg ) {

}

/* App Development */
function develApp ( arg ) {

}

/* App Initializer */
function setupApp ( arg ) {
    prompt([
        {
            type    : 'input',
            name    : 'env',
            message : 'Environment',
            default : 'development'
        }
    ], function ( configs ) {
        console.log(configs);
    });
}

/* App Configuration getter/setter */
function configApp ( arg ) {
    var keys = Object.keys(arg);

    if ( keys.length < 1 ) {
        console.log(pkgInfo);
    }
    else {
        if ( keys.length === 1 ) {
            console.log(pkgInfo.$get(keys[ 0 ]));
        }
    }
}

/* Assets Compiler */
function compileAssets ( arg ) {

}

/* Maintener */
function liftDownApp ( arg ) {

}

/* Release Maintenance */
function liftUpApp ( arg ) {

}

/* Application Updater */
function updateApp ( arg ) {

}