/**
 * Application Maintenance Server
 * A simple server to handle request during full maintenance.
 */
"use strict";

var app = require('express')();

app.use(function ( req, res ) {
    console.log(req.headers[ 'accept-language' ]);

    res.status = 500;
    res.end();
});

app.listen(8038);