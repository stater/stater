/**
 * Application Bootstrapper
 * Bootstarpper will load required things before application is being initialized.
 */
'use strict';

/* Client App Location */
var appdir;

/* Loading Sails Module */
var sails = require('sails');

/* Creating Configs */
var configs, manifest, usercfg, localcfg, routers;

/* Exporting Bootstrapper */
module.exports = {
    /* Application Initializer */
    init : function bootInit () {
        console.log('Initializing application');

        /* Populating CLI Arguments */
        getCLIArgs();

        /* Populating Configs */
        getConfigs();

        /* Populating Routers */
        getRouters();
    },

    /* Application Starter */
    start : function bootExec () {
        console.log('Application started');
    }
};

/* CLI Arguments Collector */
function getCLIArgs () {
    var args = process.argv.slice(2), parsed = {};

    /* Iterate each arguments to create mapped arguments */
    args.$each(function ( arg ) {
        arg = arg.split('=');

        /* Create Name */
        var name = arg[ 0 ].replace(/^[\-]+/, '');

        if ( arg.length > 1 ) {
            parsed[ name ] = arg[ 1 ];
        }
        else {
            parsed[ name ] = true;
        }
    });

    /* Export CLI Arguments to global */
    extend('CLI_ARG', parsed);
}

/* Core Config Collector */
function getConfigs () {
    if ( !CLI_ARG[ 'client-dir' ] ) return;

    /* Updating Client Location */
    appdir = CLI_ARG[ 'client-dir' ] + '/';

    process.chdir(appdir);

    /* Getting User Configs */
    if ( File.isDirExist(appdir + 'app/configs') ) {
        usercfg = include(appdir + 'app/configs');

        /* Removing View Engine from User Configs */
        usercfg.$del('views');
    }
    else {
        usercfg = {};
    }

    /* Getting Default Configs */
    configs = include('defaults');

    /* Getting Manifest */
    if ( File.isExist(appdir + 'manifest.json') ) {
        manifest = require(appdir + 'manifest.json');
    }
    else {
        manifest = {};
    }

    /* Getting Local Configs */
    if ( File.isExist(appdir + '.env.local') ) {
        localcfg = require(appdir + '.env.local');
    }
    else {
        localcfg = {};
    }

    /* Merging configs with user configs execpt ignored paths */
    configs.$join([ usercfg, localcfg ], [ 'views' ]);
}

/* Router Collector */
function getRouters () {
    routers = include(appdir + 'app/routers');

    routers.$each(function ( name, group ) {
        //console.log(name);
    });
}

