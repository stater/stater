(function JSFix ( jsroot ) {
    'use strict';

    /* Creating Object Extensions */
    var patches = {
        /* Path Getter */
        $get : function ( path ) {
            if ( !'string' === typeof path ) return;

            /* Define current scope, paths list, result and done status */
            var current = this, paths = path.split(this.__delimiter || '.'), result, done;

            /* Iterate deeply until done */
            while ( !done && paths.length > 0 ) {
                /* Define next object */
                var next = paths[ 0 ];

                if ( paths.length <= 1 ) {
                    /* Check last path and adding result if exist */
                    if ( current[ next ] ) {
                        result = current[ next ];
                    }
                }
                else {
                    /* Continue if next target is exist */
                    if ( 'object' === typeof current[ next ] ) {
                        /* Update current scope */
                        current = current[ next ];
                    }

                    /* Escape in first not found */
                    else {
                        done = true;
                    }
                }

                /* Define next path by slicing the paths list */
                paths = paths.slice(1);
            }

            /* Returning the result */
            return result;
        },

        /* Path Setter */
        $set : function ( path, value ) {
            if ( !'string' === typeof path ) return;

            /* Define current scope and paths list */
            var current = this, paths = path.split(this.__delimiter || '.');

            /* Iterate scopes until done */
            while ( paths.length > 0 ) {
                /* Define next target */
                var next = paths[ 0 ];

                /* Apply the value if current path is the last path */
                if ( paths.length <= 1 ) {
                    current[ next ] = value;
                    current         = current[ next ];
                }
                /* Continue to iterate if still have next path */
                else {
                    if ( 'object' === typeof current[ next ] ) {
                        /* Use next scope if exist and updating current scope */
                        current = current[ next ];
                    }
                    else {
                        /* Create next scope if not exist and updating current scope */
                        current[ next ] = {};
                        current         = current[ next ];
                    }
                }

                /* Define next path by slicing paths list */
                paths = paths.slice(1);
            }

            /* Return the object it self */
            return this;
        },

        /* Function to push item into array in path */
        $add : function ( path, value ) {
            if ( Array.isArray(this.$get(path)) ) {
                var current = this, paths = path.split(this.__delimiter || '.');

                while ( paths.length > 0 ) {
                    current = current[ paths[ 0 ] ];
                    paths   = paths.slice(1);
                }

                current.push(value);
            }

            return this;
        },

        /* Object property remover and Array item remover */
        $del : function ( path, length ) {
            /* Define current scope, paths list and done stat */
            var current = this, paths = path.split(this.__delimiter || '.'), done;

            /* Iterate each path until done */
            while ( !done && paths.length > 0 ) {
                /* Define next path */
                var next = paths[ 0 ];

                /* Last iteration */
                if ( paths.length <= 1 ) {
                    /* Delete target using object/array method if exist */
                    if ( current[ next ] ) {
                        if ( Array.isArray(current) ) {
                            current.splice(next, length || 1);
                        }
                        else {
                            delete current[ next ];
                        }
                    }
                }
                else {
                    if ( 'object' === typeof current[ next ] ) {
                        /* Update current scope if next target is exist */
                        current = current[ next ];
                    }
                    else {
                        /* Escape if not found */
                        done = true;
                    }
                }

                /* Define next path by slicing paths list */
                paths = paths.slice(1);
            }

            return this;
        },

        /* Object and Array Path Maps Extractor */
        $dir : function () {
            /* Define current path scope and path list */
            var current = '', maps = {};

            /* Perform Extract */
            extract(this);

            /* Creating Extractor */
            function extract ( target ) {
                /* Iterating each items and properties */
                target.$each(function ( a, b ) {
                    /* Copy last path */
                    var last = current;

                    /* Defining key and value by checking the target type */
                    var key = Array.isArray(target) ? b : a,
                        val = Array.isArray(target) ? a : b;

                    /* Creating new path */
                    var path = current + (!current ? '' : '.') + key;

                    /* Adding path to maps */
                    maps[ path ] = {
                        type : Array.isArray(val) ? 'array' : typeof val,
                        body : val
                    };

                    /* Extract child scope if the current scope is object or array */
                    if ( 'object' === typeof val ) {
                        /* Updating current path */
                        current = path;

                        /* Perform Extract */
                        extract(val);

                        /* Revert the current path to bring back the scope */
                        current = last;
                    }
                });
            }

            /* Returning Maps */
            return maps;
        },

        /* Object and Array Iterator */
        $each : function ( handler, reverse ) {
            if ( !'function' === typeof handler ) return;

            /* Decide to use Array iterator or Object iterator */
            if ( Array.isArray(this) ) {
                var i, ln;

                if ( !reverse ) {
                    /* Iterating each items */
                    for ( i = 0, ln = this.length; i < ln; ++i ) {
                        /* Apply this object to the handler */
                        handler.call(this, this[ i ], i);
                    }
                }
                else {
                    for ( i = this.length; i > 0; --i ) {
                        handler.call(this, this[ (i - 1) ], (i - 1));
                    }
                }
            }
            else {
                /* Iterating each properties */
                for ( var key in this ) {
                    if ( this.hasOwnProperty(key) ) {
                        /* Apply this object to the handler */
                        handler.call(this, key, this[ key ]);
                    }
                }
            }

            return this;
        },

        /* Safe Merge Object and Array merger */
        $join : function ( sources, ignore ) {
            /* Skip if sources is not an object or array */
            if ( !'object' === typeof sources ) return;

            /* Wrap source to array if not an array */
            if ( 'object' === typeof sources && !Array.isArray(sources) ) {
                sources = [ sources ];
            }

            /* Create new array if ignore is not an array */
            if ( !Array.isArray(ignore) ) ignore = [];

            /* Creating target holder and target maps */
            var main = this, self = this.$dir();

            /* Iterate each sources */
            for ( var i = 0; i < sources.length; ++i ) {
                /* Creating source holder and source maps */
                var base = sources[ i ], next = base.$dir(), igm = '??';

                /* Continue if type of target is equal to type of source */
                if ( (Array.isArray(main) && Array.isArray(base)) || (!Array.isArray(main) && !Array.isArray(base)) ) {
                    /* Iterate each maps to do merge */
                    next.$each(function ( path, value ) {
                        /* Return if path is ignored */
                        if ( ignore.indexOf(path) > -1 ) {
                            igm = path + '.';

                            return;
                        }

                        /* Ignore childs from first ignored path */
                        if ( (igm + path.split(igm)[ 1 ]) === path ) {
                            return;
                        }
                        else {
                            igm = '??';
                        }

                        /* Create new property if not exist */
                        if ( !self[ path ] ) {
                            main.$set(path, value.body);
                        }
                        else {
                            /* Replace with new value if type of next target value is different with type of next target value */
                            if ( self[ path ].type !== value.type ) {
                                main.$set(path, value.body);
                            }
                            else {
                                /* Replace if type of next target is not object or array */
                                if ( value.type !== 'object' && value.type !== 'array' ) {
                                    main.$set(path, value.body);
                                }
                            }
                        }
                    });
                }
            }

            return this;
        },

        /* Object and Array Sorter (Recursive) */
        $sort : function ( handler ) {
            /* Perform Sorting */
            return sort(this);

            /* Creating Sorter */
            function sort ( target ) {
                /* Creating result */
                var result;

                /* Array Sorter */
                if ( Array.isArray(target) ) {
                    /* Create array as result */
                    result = [];

                    /* Sort and iterate each item in target to sort the childs before adding to result */
                    target.sort(handler).$each(function ( value, i ) {
                        if ( 'object' !== typeof value ) {
                            /* Add to result if child is not object or array */
                            result[ i ] = value;
                        }
                        else {
                            /* Sort child before adding to result */
                            result[ i ] = sort(value);
                        }
                    });
                }

                /* Object Sorter */
                else {
                    /* Create object as result */
                    result = {};

                    /* Create target keys list to perform sort and iterate eachitem to sort the childs before adding to result */
                    Object.keys(target).sort(handler).$each(function ( key ) {
                        /* Creating child from target */
                        var value = target[ key ];

                        if ( 'object' !== typeof value ) {
                            /* Add to result if child is not object or array */
                            result[ key ] = value;
                        }
                        else {
                            /* Sort child before adding to result */
                            result[ key ] = sort(value);
                        }
                    });

                    /* Delete all properties from target */
                    target.$each(function ( key ) {
                        delete target[ key ];
                    });

                    /* Apply sorted properties to target */
                    result.$each(function ( key, value ) {
                        target[ key ] = value;
                    });
                }

                /* Return the target it self */
                return target;
            }
        },

        /* Object.keys wrapper */
        $keys : function () {
            if ( !Array.isArray(this) ) {
                return Object.keys(this);
            }

            return this;
        },

        /* Group Arrays */
        $wrap : function ( column, mode ) {
            if ( !Array.isArray(this) || !'number' === typeof column ) return;

            /* Create group and current index */
            var group = [],
                currn = 0;

            /* Prepare Columns */
            (column - 1).$each(function ( i ) {
                group.push([]);
            });

            /* Start Grouping */
            if ( mode === 'split' ) {
                var gpn = Math.ceil(this.length / column),
                    crg = 1;

                this.$each(function ( val ) {
                    group[ currn ].push(val);

                    if ( crg === gpn ) {
                        crg = 1;
                        currn++;
                    }
                    else {
                        crg++;
                    }
                });
            }
            else if ( mode === 'chunk' ) {
                /* Create child group */
                var childGroup = [];

                /* Reset parent group */
                group = [];

                this.$each(function ( val, i ) {
                    /* Push current value to child group */
                    childGroup.push(val);

                    if ( currn === (column - 1) || i === (this.length - 1) ) {
                        /* Add child group to parent group */
                        group.push(childGroup);

                        /* Reste child group */
                        childGroup = [];

                        /* Reset child index */
                        currn = 0;
                    }
                    else {
                        /* Increase child index */
                        currn++;
                    }
                });
            }
            else {
                this.$each(function ( val ) {
                    /* Push value to current child */
                    group[ currn ].push(val);

                    if ( currn === (column - 1) ) {
                        /* Reset current child index */
                        currn = 0;
                    }
                    else {
                        /* Increase current child index */
                        currn++;
                    }
                });
            }

            return group;
        },
    }

    /* String Patches */
    var strPatch = {
        /* Get lower string at index */
        lowerAt : function ( idx ) {
            idx = idx > 26 ? 26 : idx;

            return String.fromCharCode(97 + (idx - 1));
        },

        /* Get upper string at index */
        upperAt : function ( idx ) {
            idx = idx > 26 ? 26 : idx;

            return String.fromCharCode(65 + (idx + 1));
        }
    }

    /* String Prototypes */
    var strPro = {
        /* String Iterator */
        $each : function ( handler, reverse ) {
            if ( !'function' === typeof handler ) return;

            var i, ln;

            if ( !reverse ) {
                for ( i = 0, ln = this.length; i < ln; ++i ) {
                    handler.call(this, this[ i ], i);
                }
            }
            else {
                for ( i = this.length; i > 0; --i ) {
                    handler.call(this, this[ i - 1 ], (i - 1));
                }
            }

            return this;
        },
    }

    /* Number Patches */
    var nbrPatch = {
        /* Generate Random Number Between Two Number */
        random : function ( start, end ) {
            return Math.floor(Math.random() * (end - start + 1)) + start;
        }
    }

    /* Number Prototypes */
    var nbrPro = {
        /* Number Iterator */
        $each : function ( handler, reverse ) {
            if ( !'function' === typeof handler ) return;

            var i;

            if ( !reverse ) {
                for ( i = 0; i <= this; ++i ) {
                    handler.call(this, i);
                }
            }
            else {
                for ( i = this; i >= 0; --i ) {
                    handler.call(this, i);
                }
            }

            return this;
        }
    }

    /* Patch Targets */
    var targets = [
        {
            owner : Object.prototype,
            lists : patches
        },
        {
            owner : String,
            lists : strPatch
        },
        {
            owner : String.prototype,
            lists : strPro
        },
        {
            owner : Number,
            lists : nbrPatch
        },
        {
            owner : Number.prototype,
            lists : nbrPro
        }
    ]

    /* Applying Object Extensions */
    targets.forEach(function ( patch ) {
        for ( var key in patch.lists ) {
            /* Locking Extension */
            Object.defineProperty(patch.owner, key, {
                enumerable : false,
                value      : patch.lists[ key ]
            });
        }
    });

    jsroot.JSFix = true;
})('undefined' !== typeof global ? global : window);
