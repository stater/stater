"use strict";

/* Export Patches */
File.isExist    = fileExist;
File.isDirExist = dirExist;

/* Check does file exist */
function fileExist ( path ) {
    var exist = File.statSync(path);

    if ( exist && exist.isFile() ) return true;

    return false;
}

/* Check does directory exist */
function dirExist ( path ) {
    var exist = File.statSync(path);

    if ( exist && exist.isDirectory() ) return true;

    return false;
}