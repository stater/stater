/**
 * Main Application Module
 * This module is a file where the app life in.
 */

// Loading Importer.
require('singclude');

/* Exporting Core Variables */
extend({
    /* Root Server */
    ROOT_SVR : process.cwd()
});

/**
 * Loading core modules.
 * Those modules will be available to global, so sub modules doesn't need to re-require them.
 */
include.global({
    File : 'fs',
    Util : 'util'
});

/**
 * Loading Native Patches.
 * Native Patches doesn't have any return value, its only patching the Native Modules.
 */
include('patches/jsfix');
include('patches/file');

/**
 * Loading Helpers
 * Helpers is the core modules that mostly used by the core application.
 */
include('helpers/cli');
include('helpers/logger');

/**
 * Loading external modules.
 * External modules also will be available to global.
 */
include.global({
    Lodash : 'sails/node_modules/lodash',
    Async  : 'sails/node_modules/async'
});

// Bootstraping Application
var boot = include('bootstrap');

// Initializing Application.
boot.init();

// Starting Application.
boot.start();
