(function ( global ) {
    "use strict";

    /* Require CLI Color */
    var clr = require('colors/safe');

    /* Collecting CLI Arguments */
    var proc = process, args = proc.argv.slice(2);

    /* CLI Constructor */
    var CLI = function ( options ) {
        options = !options ? {} : options;

        this.origin   = args.join(' ');
        this.base     = args;
        this.options  = {};
        this.commands = {};
        this.parsed   = {
            arg : {},
            cmd : (args[ 0 ] || '').replace(/^[-]+/, '')
        };

        this.name    = options.name || 'Stater';
        this.about   = options.about || 'Unified NodeJS Framework. A one place to pair the front-end and back-end development.';
        this.version = options.version || 'v1.0.0';
        this.usage   = options.usage || 'stater [command] [options...]';

        delete options.name;
        delete options.about;
        delete options.version;
        delete options.usage;

        /* Wrap this object */
        var self = this;

        /* Adding options and adding default options */
        if ( 'object' === typeof options ) {
            options.$each(function ( key, option ) {
                if ( option.type === 'command' ) {
                    self.commands[ key ] = option;
                }
                else {
                    self.options[ key ] = option;

                    if ( option.defl ) {
                        self.parsed.arg[ key ] = option.defl;
                    }
                }
            });
        }

        /* Parsing Arguments */
        if ( args.length < 1 ) {
            this.$help().$done();

            return this;
        }

        /* Collecting user arguments */
        args.slice(1).$each(function ( value ) {
            var arg = value.replace(/^[-]+/, '').split('='), key = arg[ 0 ], val = arg[ 1 ];

            /* Converting Value Type */
            if ( Number(val) ) {
                val = Number(val);
            }
            else if ( val === 'true' ) {
                val = true;
            }
            else if ( val === 'false' ) {
                val = false;
            }

            if ( 'undefined' === typeof val ) {
                val = true;
            }

            /* Appying value */
            self.parsed.arg[ key ] = val;
        });

        this.$start();

        return this;
    }

    /* CLI Prototypes */
    CLI.prototype = {
        /* Command Checker */
        $start : function () {
            /* Show help */
            if ( this.parsed.cmd === 'h' || this.parsed.cmd === 'help' ) {
                this.$help().$done();
            }
            /* Show version */
            else if ( this.parsed.cmd === 'v' || this.parsed.cmd === 'version' ) {
                console.log(this.version || '');
            }
            /* Check if command required and defined */
            else {
                /* Get command from command list */
                var cmd = this.commands[ this.parsed.cmd ];

                /* Call if exist */
                if ( cmd && cmd.exec ) {
                    cmd.exec.call(this, this.parsed.arg);
                }
                else {
                    /* Check if required if not exist to print the error and helps and then exit */
                    if ( Object.keys(this.commands).length > 0 ) {
                        this.$help();

                        console.log(clr.red('Invalid command: ') + clr.green(this.parsed.cmd) + ' from ' + clr.gray(this.origin));

                        this.$done();
                    }
                    else {
                        /* Call handler if exist */
                        if ( this.handler ) {
                            this.handler.call(this, this.parsed.arg);
                        }
                    }
                }
            }

            return this;
        },

        /* Show Helps */
        $help : function () {
            /* Get longest command and argument */
            var longest = 0;

            /* Set the longest command and argument */
            this.commands.$each(function ( key ) {
                if ( key.length > longest ) longest = key.length;
            });
            this.options.$each(function ( key ) {
                if ( key.length > longest ) longest = key.length;
            });

            /* Creating Helpers */
            var helpers = {
                '-h | --help'    : 'Show helps',
                '-v | --version' : 'Show version'
            }

            helpers.$each(function ( key ) {
                if ( key.length > longest ) longest = key.length;
            });

            /* Increase longest command and argument */
            longest += 8;

            /* Space maker */
            var space = function ( from, pref ) {
                var spc = (longest - from.length), txt = clr.bold(from);

                for ( var i = 0; i < spc; ++i ) {
                    txt += ' ';
                }

                return (!pref ? '' : pref) + txt;
            }

            /* Print Header */
            console.log('');
            console.log('          -------       ||||||||       -------');
            console.log('        -------   ••••••••••••••••••••   -------');
            console.log('      -------   ••••••••••••••••••••••••   -------');
            console.log('    -------   ••••••••••••••••••••••••••••   -------');
            console.log('  -------   ••••••  S  T  A  T  E  R  ••••••   -------');
            console.log('    -------   ••••••••••••••••••••••••••••   -------');
            console.log('      -------   ••••••••••••••••••••••••   -------');
            console.log('        -------   ••••••••••••••••••••   -------');
            console.log('          -------       ||||||||       -------');

            console.log('');

            /* Print App Info */
            console.log('  ' + clr.bold(this.name));
            console.log('  ' + clr.gray(this.about));
            console.log('  ' + clr.gray(this.version));
            console.log('\r\n  Usage: ' + clr.green(this.usage));

            /* Print Available Commands */
            if ( Object.keys(this.commands).length > 0 ) {
                console.log('\r\n  ' + 'COMMANDS\r\n  ----------');

                this.commands.$each(function ( key, option ) {
                    console.log('  ' + space(key) + option.info.replace('\r\n', space('', '\r\n  ')) + '.');

                    if ( option.help ) {
                        console.log('  ' + space('') + 'USAGE: ' + clr.green(option.help.replace('\r\n', space('', '\r\n  '))));
                    }
                });
            }

            /* Print Available Options */
            if ( Object.keys(this.options).length > 0 ) {
                console.log('\r\n  ' + 'OPTIONS\r\n  ----------');

                this.options.$each(function ( key, option ) {
                    console.log('  ' + space((key.length > 1 ? '--' : '-') + key) + option.info.replace('\r\n', space('', '\r\n  ')) + ('undefined' !== typeof option.defl ? '. Default: ' + clr.gray(option.defl) : ''));
                });
            }

            /* Print Helper */
            console.log('\r\n  ' + 'HELPERS\r\n  ----------');

            helpers.$each(function ( key, value ) {
                console.log('  ' + space(key) + value);
            });

            console.log('');

            return this;
        },

        /* Destroyer */
        $done : function () {
            proc.exit();
        }
    }

    /* Bind to Global object */
    global.extend({
        CLI : CLI
    });
})('undefined' !== typeof global ? global : window);
