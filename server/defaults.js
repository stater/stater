"use strict";

module.exports = {
    /* Default Blueprint Config */
    blueprints : {
        prefix       : '/api',
        defaultLimit : 20
    },

    /* Default Global Config */
    globals : {
        sails : true
    },

    /* Default HTTP Configs */
    http : {
        middleware : {
            order : [
                'reqLogger',
                'startRequestTimer',
                'cookieParser',
                'session',
                'bodyParser',
                'handleBodyParserError',
                'compress',
                'methodOverride',
                'poweredBy',
                '$custom',
                'router',
                'www',
                'favicon',
                '404',
                '500'
            ],

            reqLogger : function ( req, res, next ) {
                console.log("Requested :: ", req.method, req.url);
                return next();
            }
        },

        cache : 31557600000,
    },

    /* Default View Config */
    views : {
        engine : renderViews
    }
}

/* View Renderer */
function renderViews ( path, data, next ) {
    /* Loading Nunjucks */
    var engine = require('nunjucks');

    if ( APPENV === 'development' ) {
        engine.configure({
            noCache : true
        });
    }

    /* Compile the */
    return engine.renderFile(path, data, next);
}